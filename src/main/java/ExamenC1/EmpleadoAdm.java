/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ExamenC1;

/**
 *
 * @author jared
 */
public class EmpleadoAdm extends Empleado {
    private int tipoContrato;

    public EmpleadoAdm(int tipoContrato, int numEmpleado, String nombre, String domicilio, float pagoDia, int diasTrabajados, float impuestoISR) {
        super(numEmpleado, nombre, domicilio, pagoDia, diasTrabajados, impuestoISR);
        this.tipoContrato = tipoContrato;
    }

    public int getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(int tipoContrato) {
        this.tipoContrato = tipoContrato;
    }
    
    public void retencion() {
        
    }
    
}
