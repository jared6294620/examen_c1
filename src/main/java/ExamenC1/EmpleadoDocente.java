/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ExamenC1;

/**
 *
 * @author jared
 */
public class EmpleadoDocente extends Empleado {
    private int nivelEstudios;

    public EmpleadoDocente(int nivelEstudios, int numEmpleado, String nombre, String domicilio, float pagoDia, int diasTrabajados, float impuestoISR) {
        super(numEmpleado, nombre, domicilio, pagoDia, diasTrabajados, impuestoISR);
        this.nivelEstudios = nivelEstudios;
    }

    public int getNivelEstudios() {
        return nivelEstudios;
    }

    public void setNivelEstudios(int nivelEstudios) {
        this.nivelEstudios = nivelEstudios;
    }
    
    public void pagoAdicional() {
        
    }
}
