/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ExamenC1;

/**
 *
 * @author jared
 */
public class Empleado {
    protected int numEmpleado;
    protected String nombre;
    protected String domicilio;
    protected float pagoDia;
    protected int diasTrabajados;
    protected float impuestoISR;

    public Empleado(int numEmpleado, String nombre, String domicilio, float pagoDia, int diasTrabajados, float impuestoISR) {
        this.numEmpleado = numEmpleado;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.pagoDia = pagoDia;
        this.diasTrabajados = diasTrabajados;
        this.impuestoISR = impuestoISR;
    }
    
    public Empleado() {
        this.numEmpleado = 0;
        this.nombre = "";
        this.domicilio = "";
        this.pagoDia = 0.0f;
        this.diasTrabajados = 0;
        this.impuestoISR = 0.0f;
    }

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public float getPagoDia() {
        return pagoDia;
    }

    public void setPagoDia(float pagoDia) {
        this.pagoDia = pagoDia;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }

    public float getImpuestoISR() {
        return impuestoISR;
    }

    public void setImpuestoISR(float impuestoISR) {
        this.impuestoISR = impuestoISR;
    }
    
    public void calcularTotal() { 
         
     }
    
    public void calcularDescuento() {
        
    }
}
